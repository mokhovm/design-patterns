﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appointment1
{
    public interface IShippingStrategy
    {
        double calculate(Order order);
        string getName();
    }
}
