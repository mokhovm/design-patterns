﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Appointment1
{
    public partial class Form1 : Form
    {
        private Random rnd;
        private ShapeFactory factory = new ShapeFactory();
        private List<ShapeData> shapes = new List<ShapeData>();
        private BufferedGraphicsContext context;
        // буффер для рисования графики
        public BufferedGraphics buffer;

        public Form1()
        {
            InitializeComponent();
            context = BufferedGraphicsManager.Current;
            Graphics g = this.CreateGraphics();
            buffer = context.Allocate(g, new Rectangle(0, 0, this.Width, this.Height));
            rnd = new Random();

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            draw();
        }

        private void draw()
        {
            buffer.Graphics.Clear(Color.Black);
            foreach (var shape in shapes)
            {
                shape.point.X += shape.direction.X;
                shape.point.Y += shape.direction.Y;
                if (shape.point.X < 0 || shape.point.X > this.Width) shape.direction.X = -shape.direction.X;
                if (shape.point.Y < 0 || shape.point.Y > this.Height) shape.direction.Y = -shape.direction.Y;
                factory.getShape(shape.aType)?.draw(buffer.Graphics, shape.point.X, shape.point.Y);
            }

            buffer.Render();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            shapes.Add(new ShapeData(rnd.Next(1, this.Width), rnd.Next(1, this.Height), typeof(MyRectangle)));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            shapes.Add(new ShapeData(rnd.Next(1, this.Width), rnd.Next(1, this.Height), typeof(MyCircle)));
        }

    }

   




}
