﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Appointment2
{
    #region Задание
    /*
     * Реализовать шаблон «Одиночка» для многопоточной программы с использованием класса Lazy < T > .
     * 
     * В этой реализации я сравнил несколько способов использования класса Lazy<> и пришел к выводу, что без 
     * использования критической секции отложенная инициализация будет либо потоконебезопасной, либо не будет отложенной.
     * 
     */
    #endregion

    /// <summary>
    /// Базовый класс для синглтона для удобства сравнения реализаций
    /// </summary>
    class BaseSinleton
    {
        /// Таймаут для эмуляции длительной инициализации
        /// Требуется настройка в зависимости от производительности компьютера
        protected const int TIMEOUT = 100;
        public string name;

        public BaseSinleton()
        {
            name = new Random().Next(0, 99999).ToString();
        }

        public static BaseSinleton getInstance()
        {
            Console.WriteLine("BaseSinleton");
            return null;
        }
    }

    /// <summary>
    /// Этот класс не является потокобезопасным, не смотря на параметр true в конструктое Lazy<MySingleton1>(true);
    /// </summary>
    class MySingleton1 : BaseSinleton
    {
        private static Lazy<MySingleton1> m_instance;

        public new static MySingleton1 getInstance()
        {
            if (m_instance == null)
            {
                Thread.Sleep(TIMEOUT);
                m_instance = new Lazy<MySingleton1>(true);
            }
            Console.WriteLine(new StackFrame().GetMethod().DeclaringType + " " + m_instance.Value.name);
            return m_instance.Value;
        }
    }

    /// <summary>
    /// Этот класс потокобезопасный за счет readonly, но в чем тогда преимущество Lazy инициализации
    /// </summary>
    class MySingleton2 : BaseSinleton
    {
        private static readonly Lazy<MySingleton2> m_instance = new Lazy<MySingleton2>(true);

        public new static MySingleton2 getInstance()
        {
            Thread.Sleep(TIMEOUT);
            Console.WriteLine(new StackFrame().GetMethod().DeclaringType + " " + m_instance.Value.name);
            return m_instance.Value;
        }
    }


    /// <summary>
    /// По-настоящему потокобезопасную инициализацию можно сделать только через критическую секцию
    /// </summary>
    class MySingleton3 : BaseSinleton
    {
        private static Lazy<MySingleton3> m_instance;
        private static readonly object syncObj = new object();

        public new static MySingleton3 getInstance()
        {
            lock (syncObj)
            {
                if (m_instance == null)
                {
                    Thread.Sleep(TIMEOUT);
                    m_instance = new Lazy<MySingleton3>(true);
                }
            }
            Console.WriteLine(new StackFrame().GetMethod().DeclaringType + " " + m_instance.Value.name);
            return m_instance.Value;
        }
    }

    internal class Program
    {
        /// <summary>
        /// Тестируем разные реализации отложенной инициализации
        /// </summary>
        /// <param name="caption">описание решения</param>
        /// <param name="className">имя класса для решения</param>
        private static void Test(string caption, string className)
        {
            Console.WriteLine();
            Console.WriteLine(caption);
            for (int i = 0; i < 5; i++)
            {
                new Thread(() =>
                    {
                        // по имени класса получим ссылку на его класс и вызовем метод getInstance();
                        Type type = Type.GetType(typeof(BaseSinleton).Namespace + "." + className);
                        if (type != null) type.GetMethod("getInstance").Invoke(null, null);
                    }
                ).Start();
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            Test("ПотокоНЕбезопасная отложенная инициализация", "MySingleton1");
            Test("Потокобезопасная, но не отложенная инициализация", "MySingleton2");
            Test("Потокобезопасная отложенная инициализация", "MySingleton3");
        }
    }
}
