﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Пример принципа открытости/закрытости
/// Прин­цип откры­то­сти / закры­то­сти декларирует, что про­грамм­ные сущ­но­сти (клас­сы, моду­ли, функ­ции и т. п.) 
/// должны быть открыты для рас­ши­ре­ния, но закрыты для изме­не­ния. Это озна­ча­ет, что эти сущ­но­сти могут менять свое пове­де­ние без изме­не­ния их исход­ного кода.
/// В данном примере репозиторий заказов может работать с разними источниками данных без изменения реализации.
/// </summary>
namespace OpenClose
{
    /// <summary>
    /// Интерфейс хранилища
    /// </summary>
    interface IMyDataLink
    {
        Order Load(long orderId);
        void Save(Order order);
        void Update(Order order);
        void Delete(Order order);
        string GetName();
    }

    /// <summary>
    /// Абстрактный класс, от которого должны наследоваться хранилища
    /// </summary>
    abstract class CustomLink : IMyDataLink
    {
        public string Name { get; set; }

        public virtual void Delete(Order order)
        {
            Console.WriteLine($"{Name}: delete order {order.Id}" );
        }

        public string GetName()
        {
            return Name;
        }

        public virtual Order Load(long orderId)
        {
            Console.WriteLine($"{Name}: Load order {orderId}");
            return new Order {Id = orderId, Name = $"{Name}_order"};
        }

        public virtual void Save(Order order)
        {
            Console.WriteLine($"{Name}: Save order {order.Id}");
        }

        public virtual void Update(Order order)
        {
            Console.WriteLine($"{Name}: update order {order.Id}");
        }
    }

    /// <summary>
    /// Конкретное хранилище на базе MySql
    /// Методы надо будет переопределить для конкретной реализации
    /// </summary>
    class MySqlLink : CustomLink
    {
        public MySqlLink() : base()
        {
            Name = "MySqlLink";
        }
    }

    /// <summary>
    /// Конкретное хранилище где-то в вебе
    /// Методы надо будет переопределить для конкретной реализации
    /// </summary>
    class WebLink : CustomLink
    {
        public WebLink() : base()
        {
            Name = "WebLink";
        }
    }

    /// <summary>
    /// заказ
    /// </summary>
    class Order
    {
        public long Id;
        public string Name;
    }


    /// <summary>
    /// Репозиторий заказов
    /// </summary>
    class OrderRepository
    {
        private IMyDataLink _dataLink;

        public OrderRepository (IMyDataLink dataLink)
        {
            _dataLink = dataLink;
            Console.WriteLine($"plugin datalink {_dataLink.GetName()}");
        }

        public Order Load(long orderId)
        {
            return _dataLink.Load(orderId);
        }

        public void Save(Order order)
        {
            _dataLink.Save(order);
        }

        public void Update(Order order)
        {
            _dataLink.Update(order);
        }

        public void Delete(Order order)
        {
            _dataLink.Delete(order);
        }

        public void Test(long id)
        {
            var order = Load(id);
            Update(order);
            Delete(order);
            Save(order);
        }
    }

    class Program
    {
        static void Tests()
        {
            OrderRepository repo;
            var link1 = new MySqlLink();
            repo = new OrderRepository(link1);
            repo.Test(1);

            var link2 = new WebLink();
            repo = new OrderRepository(link2);
            repo.Test(2);
        }

        static void Main(string[] args)
        {
           
            Tests();
            Console.ReadKey();
        }
    }
}
