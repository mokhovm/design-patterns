﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appointment1
{
    class EmsStrategy : CustomStrategy
    {
        public EmsStrategy(string name, double baseCost) : base(name, baseCost){}

        public override double calculate(Order order)
        {
            return baseCost ;
        }
    }
}
