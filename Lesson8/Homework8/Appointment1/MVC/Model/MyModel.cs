﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appointment1.MVC.Model
{
    
    class MyModel : IMyModel
    {
        public event EventHandler onChanged;
        
        protected List<string> itemList;

        public MyModel()
        {
            itemList = new List<string>();
        }

        public void subscribe(IMyObserver observer)
        {
            onChanged += new EventHandler(observer.dataChanged);
        }

        public void addItem(string item)
        {
            itemList.Add(item);
            onChanged?.Invoke(this, null);
        }

        public void editItem(string item, int idx)
        {
            itemList[idx] = item;
            onChanged?.Invoke(this, null);
        }

        public void deleteItem(int idx)
        {
            itemList.RemoveAt(idx);
            onChanged?.Invoke(this, null);
        }

        public IEnumerable<string> getItems()
        {
            return itemList;
        }
    }

}
