﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Appointment1.MVC.Controller;
using Appointment1.MVC.Model;

namespace Appointment1
{

    #region Задание 1
    /*
        Используя шаблон MVC, разработать следующее приложение. На форме расположен textbox, listbox и button. 
        После ввода текста в textbox введенная строка добавляется в коллекцию значений в модели. 
        При нажатии на кнопку коллекция отображается в listbox.
    */
    #endregion
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MvcTestForm view = new MvcTestForm();
            IMyModel model = new MyModel();
            IMyController controller = new MyController(view, model);
            Application.Run(view);
        }
    }
}
