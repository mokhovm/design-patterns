﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appointment1
{
    public class Order
    {
        public Address Destination { get; set; }
        public Address Origin { get; set; }
    }
}
