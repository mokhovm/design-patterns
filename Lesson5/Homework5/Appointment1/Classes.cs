﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appointment1
{
    /// <summary>
    /// Хелпер для Random 
    /// </summary>
    public static class CommonUtils
    {
        public static int RandomFromList(this Random rnd, int[] list)
        {
            int i = new Random().Next(0, list.Length);
            return list[i];
        }
    }

    /// <summary>
    /// Внешние данные для фигуры
    /// </summary>
    class ShapeData
    {
        // движение с увеличением координаты
        private const int DIR_DIRECT = 1;
        // движение с уменьшением координаты
        private const int DIR_INVERSE = -1;
        
        public Point point = new Point();
        public Point direction = new Point();
        public Type aType;

        public ShapeData(int x, int y, Type type)
        {
            var rnd = new Random();
            point.X = x;
            point.Y = y;
            this.aType = type;
            direction.X = rnd.RandomFromList(new int[2] { DIR_INVERSE, DIR_DIRECT });
            direction.Y = rnd.RandomFromList(new int[2] { DIR_INVERSE, DIR_DIRECT });
            //Debug.WriteLine($"{direction.X}:{direction.Y}");
        }
    }

    /// <summary>
    /// Фабрика фигур
    /// Умеет рожать фигуры MyCircle и MyRectangle
    /// </summary>
    class ShapeFactory
    {
        private List<Shape> _list = new List<Shape>();

        public ShapeFactory()
        {
            _list.Add(new MyCircle());
            _list.Add(new MyRectangle());
        }

        public Shape getShape(Type shapeType)
        {
            Shape res = null;
            foreach (var shape in _list)
            {
                if (shape.GetType() == shapeType)
                {
                    res = shape;
                    break;
                }
            }
            return res;
        }
    }

    /// <summary>
    /// Базовый класс фигуры
    /// </summary>
    abstract class Shape
    {
        protected const int LINE_WIDTH = 1;
        protected Pen myPen;
        protected SolidBrush myBrush;

        public abstract void draw(Graphics g, int x, int y);
    }

    /// <summary>
    /// Круг
    /// </summary>
    class MyCircle : Shape
    {
        private int radius = 40;

        public MyCircle()
        {
            myPen = new Pen(Color.DodgerBlue, LINE_WIDTH);
            myBrush = new SolidBrush(Color.DeepSkyBlue);
        }

        override public void draw(Graphics g, int x, int y)
        {
            g.FillEllipse(myBrush, x, y, radius, radius);
            g.DrawEllipse(myPen, x, y, radius, radius);
        }
    }

    /// <summary>
    /// Прямоугольник
    /// </summary>
    class MyRectangle : Shape
    {
        private int width = 50;
        private int height = 25;

        public MyRectangle()
        {
            myPen = new Pen(Color.OrangeRed, LINE_WIDTH);
            myBrush = new SolidBrush(Color.Coral);
        }

        override public void draw(Graphics g, int x, int y)
        {
            g.FillRectangle(myBrush, x, y, width, height);
            g.DrawRectangle(myPen, x, y, width, height);
        }
    }
}
