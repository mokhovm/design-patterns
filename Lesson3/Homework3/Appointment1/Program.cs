﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Используем шаблон Фабрика для для создания объектов, описывающих геометрические фигуры (круг, квадрат, прямоугольник).
/// </summary>
namespace Appointment1
{
    class FigureFactory
    {
        private Dictionary<string, Type> figureClasses;

        /// <summary>
        /// Фабрика фигур создает инстанции классов через Reflection
        /// </summary>
        public FigureFactory()
        {
            figureClasses = new Dictionary<string, Type>();
            FindAvailableClasses();
        }

        /// <summary>
        /// Инстанциирует класс по его имени
        /// </summary>
        /// <param name="className"> имя класса </param>
        /// <param name="aColor"> цвет фигуры </param>
        /// <param name="aX"> х координата </param>
        /// <param name="aY"> у координата </param>
        /// <returns></returns>
        public IFigure CreateFigure(string className, string aColor, float aX, float aY)
        {
            IFigure res = null;
            figureClasses.TryGetValue(className, out var newClass);
            newClass = newClass ?? typeof(BaseFigure);
            res = Activator.CreateInstance(newClass, aColor, aX, aY) as IFigure;
            return res;
        }

        /// <summary>
        /// формирует список классов в этой сборке, реализующих интерфейс IFigure
        /// </summary>
        private void FindAvailableClasses()
        {
            Type[] typesInThisAssembly = Assembly.GetExecutingAssembly().GetTypes();
            foreach (Type type in typesInThisAssembly)
            {
                if (type.GetInterface(typeof(IFigure).ToString()) != null)
                {
                    figureClasses.Add(type.Name, type);
                }
            }
        }
    }

    /// <summary>
    /// Интерфейс геометрических фигур
    /// </summary>
    interface IFigure
    {
        float X { get; }
        float Y { get; }
        string Color { get; }
        string Name { get; }
    }

    /// <summary>
    /// Базовый класс для фигур
    /// </summary>
    class BaseFigure : IFigure
    {
        public float X { get; set; }
        public float Y { get; set; }
        public string Color { get; set; }
        public string Name { get; set; }

        public BaseFigure()
        {
            Name = GetType().Name;
        }

        public BaseFigure(string aColor, float aX, float aY) : this()
        {
            Color = aColor;
            X = aX;
            Y = aY;
        }

        public override string ToString()
        {
            return $"{Name} with color {Color} at {X}:{Y}";
        }
    }

    class Square : BaseFigure
    {
        public Square(string aColor, float aX, float aY) : base(aColor, aX, aY)
        {
            
        }
    }

    class Circle : BaseFigure
    {
        public Circle(string aColor, float aX, float aY) : base(aColor, aX, aY)
        {

        }
    }

    class Rectangle : BaseFigure
    {
        public Rectangle(string aColor, float aX, float aY) : base(aColor, aX, aY)
        {

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var factory = new FigureFactory();
            Console.WriteLine(factory.CreateFigure("Square", "white", 1, 1));
            Console.WriteLine(factory.CreateFigure("Rectangle", "red", 10, 4));
            Console.WriteLine(factory.CreateFigure("Circle", "blue", 2, 5));
            // треугольника у нас нет, поэтому будет создана фигура базового класса коричневого цвета
            Console.WriteLine(factory.CreateFigure("Triange", "brown", 7, 1));
            Console.ReadKey();

        }
    }
}
