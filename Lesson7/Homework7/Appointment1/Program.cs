﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appointment1
{
    #region Задание 
    //Реализовать шаблон «Стратегия» без использования делегатов.При решении задачи следует применить следующий интерфейс:
    //public interface IShippingStrategy
    //{
    //    double Calculate(Order order);
    //}
    #endregion

    class Program
    {
       
        static void Main(string[] args)
        {
            var calc = new ShippringCostCalculator();
            do
            {
                calc.showShippingTypes();
                var strShipType = Console.ReadLine();
                if (strShipType == "") break;

                calc.installStrategy(strShipType);
                if (calc.strategy != null)
                {
                    var value = calc.calculate(new Order());
                    Console.WriteLine($"Стоимость доставки {value} рублей");
                }
                else
                {
                    Console.WriteLine("Ничего не знаю про такой тип доставки.");
                }

            } while (true);

            Console.WriteLine("Расчет окончен. Спасибо за сотрудничество.");
            Console.ReadKey();
        }
    }
}
