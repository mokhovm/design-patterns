﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Appointment1.MVC.Controller;
using Appointment1.MVC.Model;

namespace Appointment1
{

    public partial class MvcTestForm : Form, IMyView, IMyObserver
    {
        private IMyController controller;
        public event EventHandler onChanged;

        public MvcTestForm()
        {
            InitializeComponent();
            btnAdd.Click += (sender, args) =>
            {
                if (textBox1.Text != "") controller.addItem(textBox1.Text);
                onChanged?.Invoke(this, EventArgs.Empty);
            };
            btnEdit.Click += (sender, args) =>
            {
                if (listBox1.SelectedIndex != -1) controller.editItem(textBox1.Text, listBox1.SelectedIndex);
                onChanged?.Invoke(this, EventArgs.Empty);
            };
            btnDelete.Click += (sender, args) =>
            {
                if (listBox1.SelectedIndex != -1) controller.deleteItem(listBox1.SelectedIndex);
                onChanged?.Invoke(this, EventArgs.Empty);
            };
        }

        public void setController(IMyController cont)
        {
            controller = cont;
        }

        public void dataChanged(object sender, EventArgs e)
        {
            // вызывается из модели, когда данные поменялись
            // тут обновляем вид данными из модели
            listBox1.Items.Clear();
            foreach (var item in (sender as IMyModel).getItems())
            {
                listBox1.Items.Add(item);
            }
            
        }


    }
}
