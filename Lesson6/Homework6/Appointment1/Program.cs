﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appointment1
{
    #region Задание 1
    /*
     Используя шаблон «Цепочка обязанностей», реализовать следующую задачу. 
     Сотрудник компании желает получить одобрение на получение некоторой суммы денег. 
     Для этого он обращается к своему непосредственному руководителю.
     Тот не имеет полномочий одобрить выделение такой суммы денег (может одобрить меньшую ) 
     и обращается к своему руководителю (директору компании). Он может одобрить или 
     отклонить запрос и отправить ответ сотруднику.
     
     */
    #endregion

    /// <summary>
    /// класс запроса
    /// </summary>
    public class Request
    {
        public int amount;

        public Request(int amount)
        {
            this.amount = amount;
        }
    }

    /// <summary>
    /// Интерфейс cогласование
    /// </summary>
    public interface IApprovable
    {
        IApprovable setNextHandler(IApprovable next);
        bool aprove(Request request);
        string getAproverName();
    }

  
    /// <summary>
    /// Абстрактный менеджер
    /// </summary>
    abstract public class CustomManager : IApprovable
    {
        protected IApprovable m_nextManager;
        protected string name;
        protected string position;
        protected int maxAmount;

        protected CustomManager(string name, string position, int maxAmount)
        {
            this.name = name;
            this.position = position;
            this.maxAmount = maxAmount;
        }

        public string getAproverName()
        {
            return name;
        }

        public IApprovable setNextHandler(IApprovable next)
        {
            m_nextManager = next;
            return this;
        }

        public virtual bool aprove(Request request)
        {
            return false;
        }
    }


    /// <summary>
    /// Конкретный менеджер
    /// </summary>
    public class Manager : CustomManager
    {
        public Manager(string name, string position, int maxAmount) : base(name, position, maxAmount)
        {
        }

        protected bool internalAprove(Request request)
        {
            return request.amount <= maxAmount;
        }

        public override bool aprove(Request request)
        {
            bool res = internalAprove(request);
            if (res)
            {
                Console.WriteLine($"{position} {name} согласовал выделение {request.amount} рублей.");
            }
            else
            {
                Console.Write($"{position} {name} НЕ согласовал выделение {request.amount} рублей");
                if (m_nextManager != null)
                {
                    Console.WriteLine($" и обратрился к {m_nextManager.getAproverName()}");
                    res = m_nextManager.aprove(request);
                }
                else
                {
                    Console.WriteLine(". Таких денег у нас нет, приходите завтра.");
                }
            }
            return res;
        }
    }



    class Program
    {
        static void Main(string[] args)
        {

            var m1 = new Manager("Иванов А. А.", "Менеджер", 1000).
                setNextHandler(new Manager("Петров Б. Б.", "Финансовый директор", 50000).
                setNextHandler(new Manager("Васечкин Т. Т.", "Руководитель компании", 250000)));

            string str;
            do
            {
                Console.WriteLine("Укажие сумму или 0 для выхода.");
                str = Console.ReadLine();
                if (int.TryParse(str, out var sum))
                {
                    var req = new Request(sum);
                    m1.aprove(req);
                }
                else 
                  Console.WriteLine("Ерунда какая-то... Попробуй ещё раз!");
            }
            while (str != "0");
        }
    }
}
