﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appointment1
{
    /// <summary>
    /// Класс предназначен для расчета стоимости доставки товара
    /// </summary>
    class ShippringCostCalculator
    {
        // смещение индекса для удобства нумерации стратегий
        private const int IDX_OFFSET = 1;
        // список для хранения доступных стратегий
        private readonly List<IShippingStrategy> m_strategies;
        private IShippingStrategy m_strategy;
        public IShippingStrategy strategy {
            get => m_strategy;
            set
            {
                if (m_strategy != value)
                {
                    m_strategy = value;
                    var msg = m_strategy == null ? $"Стратегия не установлена" : $"Установлена стратегия {m_strategy.getName()}";
                    Console.WriteLine(msg);
                }
            }
        }

        public ShippringCostCalculator()
        {
            // создадим стратегии расчета
            m_strategies = new List<IShippingStrategy> {new EmsStrategy("EMS", 10), new FedExStrategy("FedEx", 20),
                new UpsStrategy("UPS", 50)};
        }

        public double calculate(Order order)
        {
            return strategy.calculate(order);
        }

        public void installStrategy(string index)
        {
            strategy = parseShippingType(index);
        }

        public void showShippingTypes()
        {
            Console.WriteLine("Укажите способ доставки:");
            foreach (var s in m_strategies)
            {
                Console.WriteLine($"{m_strategies.IndexOf(s) + IDX_OFFSET} - {s.getName()}");
            }
            Console.WriteLine("Или Enter для выхода");

        }

        private IShippingStrategy parseShippingType(string value)
        {
            IShippingStrategy res = null;
            if (int.TryParse(value, out var index))
            {
                index -= IDX_OFFSET;
                if (index < m_strategies.Count) res = m_strategies[index];
            }
            return res;
        }
    }
}
