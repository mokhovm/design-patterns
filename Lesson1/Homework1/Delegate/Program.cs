﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate
{
    class Program
    {
        public const string ADDRESS = "Россия, Москва, Кремль";
        public const string FMT_STRING = "Имя: {0}; Описание: {1}; Адрес: {2}; Возраст: {3}";
        private static Func<string> methodCalls;
        private static string DummyFunc()
        {
            return String.Format(FMT_STRING, "Петя", "школьный друг", ADDRESS, 30);
        }
        private static string DummyFuncAgain()
        {
            return String.Format(FMT_STRING, "Вася", "сосед", ADDRESS, 54);
        }
        private static string DummyFuncMore()
        {
            return String.Format(FMT_STRING, "Николай", "сын", ADDRESS, 4);
        }
        private static void WriteToConsole(string name, string description, int age)
        {
            Console.WriteLine(FMT_STRING, name, description, ADDRESS, age);
        }

        private static void MakeAction(Action action)
        {
            string methodName = action.Method.Name;
            Console.WriteLine("Начало работы метода {0}", methodName);
            action();
            Console.WriteLine("Окончание работы метода {0}", methodName);
        }

        private static List<Func<string>> GetActionSteps()
        {
            return new List<Func<string>>()
            {
                DummyFunc,
                DummyFuncAgain,
                DummyFuncMore
            };
        }

        static void Main(string[] args)
        {
            List<Func<string>> actions = GetActionSteps();
            foreach (var action in actions) methodCalls += action;

            var results = methodCalls.GetInvocationList().Select(x => (string)x.DynamicInvoke());
            foreach (var result in results)
                Console.WriteLine(result);


            Console.ReadLine();
        }

    }
}

