﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Appointment1.MVC.Model;

namespace Appointment1.MVC.Controller
{
    /// <summary>
    /// Контроллер знает о виде и модели.
    /// Моделью может управлять
    /// От вида может получать события
    /// </summary>
    class MyController : IMyController
    {
        protected IMyView view;
        protected IMyModel model;
        public MyController(IMyView view, IMyModel model)
        {
            this.view = view;
            this.model = model;
            view.setController(this);
            view.onChanged += (sender, args) => { Debug.WriteLine("У вида что-то поменялось"); };
            model.subscribe((IMyObserver)view);
        }

        public void addItem(string item)
        {
            model.addItem(item);            
        }

        public void editItem(string item, int idx)
        {
            model.editItem(item, idx);
        }

        public void deleteItem(int idx)
        {
            model.deleteItem(idx);
        }
    }
}
