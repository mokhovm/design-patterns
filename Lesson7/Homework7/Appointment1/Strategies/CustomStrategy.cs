﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appointment1
{
    /// <summary>
    /// Базовый класс стратегий расчета доставки
    /// </summary>
    abstract class CustomStrategy : IShippingStrategy
    {
        protected string name;
        protected double baseCost;

        protected CustomStrategy(string name, double baseCost)
        {
            this.name = name;
            this.baseCost = baseCost;
        }

        public virtual double calculate(Order order)
        {
            throw new NotImplementedException();
        }

        public string getName()
        {
            return name;
        }
    }
}
