﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework1
{

    public interface IIdGenerator
    {
        long CalculateId();
    }
    public class DefaultIdGenerator : IIdGenerator
    {
        public long CalculateId()
        {
            long id = DateTime.Now.Ticks;
            return id;
        }
    }


    public class CustomEntity
    {
        public long Id { get; set; }
        public string Caption { get; set; }
        public string Decscription { get; set; }
        private IIdGenerator _generator;
        public CustomEntity(IIdGenerator generator)
        {
            Id = generator.CalculateId();
        }
        public override string ToString()
        {
            return $"id:{Id} caption:{Caption} desc:{Decscription}";
        }
    }

    public class Store : CustomEntity
    {
        public Store(IIdGenerator generator) : base(generator)
        {
            Caption = "Store1";
            Decscription = "Это магазин";
        }

      
    }
    public class Customer : CustomEntity
    {
        public Customer(IIdGenerator generator) : base(generator)
        {
            Caption = "Customer1";
            Decscription = "Это покупатель";
        }
    }



    class Program
    {
        static void Main(string[] args)
        {
            DefaultIdGenerator generator = new DefaultIdGenerator();
            Console.WriteLine(new Store(generator).ToString());
            Console.WriteLine(new Customer(generator).ToString());
            Console.ReadKey();
        }
    }
}
