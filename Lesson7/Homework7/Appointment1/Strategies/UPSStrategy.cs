﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appointment1
{
    class UpsStrategy : CustomStrategy
    {
        public UpsStrategy(string name, double baseCost) : base(name, baseCost){}

        public override double calculate(Order order)
        {
            return baseCost;
        }
    }
}
