﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Appointment1
{

    #region Задание
    /*
     * Реализовать шаблон «Одиночка» для многопоточной программы с использованием оператора lock.
     * 
     * Для реализации достаточно создать критическую секцию с помощью lock, в которой и будет происходить проверка и 
     * инициализация инстанции синглтона. Ниже представлена программа, демонстрирующая проблемы в многопоточной архитектуре, 
     * если не применять такую блокировку.
     */
    #endregion



    /// <summary>
    /// Класс Игра - для примера
    /// </summary>
    public class Game
    {
        private static Game m_instance;
        private static object syncObj = new object();

        public string Name;

        protected Game(string aName)
        {
            // эмулируем долгую инициализацию конструктора
            const int TIMEOUT = 20;
            
            Thread.Sleep(TIMEOUT);
            Name = aName;
            Console.WriteLine("create instance " + aName);
        }

        /// <summary>
        /// тестовый метод для очистки инстанции синглтона
        /// </summary>
        public static void clear()
        {
            m_instance = null;
        }

        /// <summary>
        /// Получить инстанцию
        /// </summary>
        /// <param name="aName">Имя игры</param>
        /// <param name="isUseLock">использовать ли блокировку</param>
        /// <returns></returns>
        public static Game getInstance(string aName, bool isUseLock)
        {
            if (isUseLock)
            {
                lock (syncObj)
                {
                    if (m_instance == null)
                    {
                        m_instance = new Game(aName);
                    }
                }
            }
            else
            {
                if (m_instance == null)
                {
                    m_instance = new Game(aName);
                }
            }
            return m_instance;
        }
    }

    /// <summary>
    /// Вспомогательный класс для инициализации ниток
    /// </summary>
    internal class ThreadParams
    {
        public string name;
        public bool isUseSafe;

        public ThreadParams(string name, bool isUseSafe)
        {
            this.name = name;
            this.isUseSafe = isUseSafe;
        }
    }

    internal class Program
    {
        /// <summary>
        /// Запрашивает инстанцию игры. Вызывается из нитки, поэтому параметры передаются 
        /// в переменной obj
        /// </summary>
        /// <param name="obj">Переменна типа ThreadParams для инициализации нитки</param>
        private static void createGame(object obj)
        {
            ThreadParams pref = (obj as ThreadParams);
            var MyGame = Game.getInstance(pref.name, pref.isUseSafe);
        }

        /// <summary>
        /// Тестируем синглтон
        /// </summary>
        /// <param name="useLock">Использовать ли блокировку</param>
        private static void TestSingleton(bool useLock)
        {
            const int MAX_QTY = 50;
            const string GAME_NAME1 = "Mario";
            var threads = new List<Thread>();
   
            // очистим инстанцию синглтона
            Game.clear();

            // создадим несколько потоков, обращающихся к синглтону
            for (var i = 0; i < MAX_QTY; i++)
            {
                var thread = new Thread(createGame);
                thread.Start(new ThreadParams($"{GAME_NAME1}-{i}", useLock));
                threads.Add(thread);
            }

            // дождемся завершения ниток
            foreach (var thread in threads)
            {
                thread.Join();
            }
        }

        private static void Main(string[] args)
        {
            ConsoleKeyInfo info;
            do
            {
                Console.WriteLine("Создаем синглтон без многопоточной блокировки. При этом может создаться от одной до нескольких инстанций:");
                TestSingleton(false);
                
                Console.WriteLine("Создаем синглтон с блокировкой. Всегда создается только одна инстаниця:");
                TestSingleton(true);

                Console.WriteLine("Нажмите выхода нажмите Esc или любую клавишу для продолжения...");
                info = Console.ReadKey();

            } while (info.Key != ConsoleKey.Escape);

        }
    }
}
