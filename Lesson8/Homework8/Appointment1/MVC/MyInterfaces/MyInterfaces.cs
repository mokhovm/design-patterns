﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appointment1.MVC.Model
{
    /// <summary>
    /// Интерфейс для контроллера
    /// Определяет доступные операции для пользователя
    /// </summary>
    public interface IMyController
    {
        void addItem(string item);
        void editItem(string item, int idx);
        void deleteItem(int idx);
    }

    /// <summary>
    /// Интерфейс модели
    /// Определяет механизмы управления моделью, подписку на события от модели и доступ к списку элементов
    /// </summary>
    public interface IMyModel
    {
        void subscribe(IMyObserver observer);
        void addItem(string item);
        void editItem(string item, int idx);
        void deleteItem(int idx);
        IEnumerable<string> getItems();
    }

    /// <summary>
    /// Интерфейс вида
    /// </summary>
    public interface IMyView
    {
        event EventHandler onChanged;
        void setController(IMyController cont);
    }

    /// <summary>
    /// Интерфейс для реализации паттерна Наблюдатель
    /// Предназначен для получения уведомлений об изменении модели
    /// </summary>
    public interface IMyObserver
    {
        void dataChanged(object sender, EventArgs e);
    }


}
