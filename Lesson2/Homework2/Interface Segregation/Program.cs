﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Пример принципа разделения интерфесов.
/// Принцип разделения интерфейса (Interface Segregation Principle или ISP): много специализированных интерфейсов лучше, чем один универсальный. 
/// Другими словами, слиш­ком «тол­стые» интер­фейсы необ­хо­димо раз­де­лять на более малень­кие и спе­ци­фи­че­ские, чтобы кли­енты малень­ких интер­фей­сов 
/// знали только о мето­дах, кото­рые необ­хо­димы им в рабо­те. В ито­ге, при изме­не­нии метода интер­фейса не должны меняться кли­енты, кото­рые этот метод не исполь­зуют.
/// В этом примере мы базовый интерфейс IProposed разделили на IDressing и IPlugable. Превый подходит для одежды, второй для электрических инструментов
/// </summary>
/// 
namespace Interface_Segregation
{
    /// <summary>
    /// Интерфейс любой продаваемой вещи
    /// </summary>
    interface IProposed
    {
        void SetDiscount(double discount);
        void SetPromocode(string promocode);
        void SetPrice(double price);
    }

    /// <summary>
    /// интерфейс для надеваемой одежды
    /// </summary>
    interface IDressing
    {
        void SetColor(Color color);
        void SetSize(Size size);
    }

    /// <summary>
    /// Интерфейс для подключаемой к электрической сети техники
    /// </summary>
    interface IPlugable
    {
        void SetVoltage(int voltage);
        void SetFrecurency(int frec);
    }

    /// <summary>
    /// Базовый класс для продаваемых предеметов
    /// </summary>
    class CustomItem : IProposed
    {
        public void SetDiscount(double discount) { }
        public void SetPrice(double price) { }
        public void SetPromocode(string promocode) { }
    }

    /// <summary>
    /// Одежда
    /// </summary>
    class Clothes : CustomItem, IDressing
    {
        public void SetColor(Color color) { }
        public void SetSize(Size size) { }
    }

    /// <summary>
    /// Электрические инструменты
    /// </summary>
    class ElectricTools : CustomItem, IPlugable
    {
        public void SetVoltage(int voltage) {}
        public void SetFrecurency(int frec) {}
    }


    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
