﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appointment2
{
    #region Задание 2
    /*
     * Реализовать «Заместитель» при работе с классом Math, ограничив его четырьмя функциями: +,-,*,/ .
     * 
     */
    #endregion

    /// <summary>
    /// Интрефейс мат. операций
    /// </summary>
    public interface IMath
    {
        int sum(int x, int y);
        int sub(int x, int y);
        int mult(int x, int y);
        float div(int x, int y);
    }

    /// <summary>
    /// Некий математический класс для примера
    /// </summary>
    class MyMath : IMath
    {
        public int sum(int x, int y)
        {
            return x + y;
        }

        public int sub(int x, int y)
        {
            return x - y;
        }

        public int mult(int x, int y)
        {
            return x * y;
        }

        public float div(int x, int y)
        {
            return x / y;
        }
    }

    /// <summary>
    /// прокси для класса MyMath
    /// </summary>
    class ProxyMath : IMath
    {
        // ссылка на инстранцию проксируемого класса
        private MyMath _math;
        private MyMath instance => _math ?? (_math = new MyMath());

        public int sum(int x, int y)
        {
            return instance.sum(x, y);
        }

        public int sub(int x, int y)
        {
            return instance.sub(x, y);
        }

        public int mult(int x, int y)
        {
            return instance.mult(x, y);
        }

        public float div(int x, int y)
        {
            return instance.div(x, y);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            const int x = 10;
            const int y = 2;

            ProxyMath proxy = new ProxyMath();
            Console.WriteLine("Проксируем инстанцию класса MyMath:");
            Console.WriteLine($"{x} + {y} = {proxy.sum(10, 2)}");
            Console.WriteLine($"{x} - {y} = {proxy.sub(10, 2)}");
            Console.WriteLine($"{x} * {y} = {proxy.mult(10, 2)}");
            Console.WriteLine($"{x} / {y} = {proxy.div(10, 2)}");
            Console.ReadKey();

        }
    }
}
